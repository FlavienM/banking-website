<html>
<head>
<style type=text/css>
</style>
</head>

<body>
<?php
session_start();
include_once("ManagerNav.php");
include "dbconnect.php";

/*if(! $_SESSION['Username']){
	echo "Login to access <a href= 'ManagerPage.php'> Login </a><br>";
	exit;
}*/
//$currentUserId = $_SESSION['UserId']; 

?>

<form class="form-horizontal" action="processDeposit.php">
<fieldset>

<legend>Deposit Money</legend>
<div class="form-group">
  <label class="col-md-4 control-label" for="Account">ID of Account to Deposit to</label>  
  <div class="col-md-4">
  <input id="Account" name="Account" type="number" min="1" placeholder="" required="True" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Amount">Amount</label>  
  <div class="col-md-4">
  <input id="Amount" name="Amount" type="number" placeholder="" min="1" max="1000" required="True" class="form-control input-md">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Submit"></label>
  <div class="col-md-4">
    <button id="Submit" name="Submit" class="btn btn-primary">Deposit</button>
  </div>
</div>

</fieldset>
</form>