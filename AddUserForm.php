<html>
<head>
<title> 
FA Bank
</title>
</head>

<body>
<?php
include_once("navbar.php");
?>

<form class="form-horizontal" action = "AddUser.php">
<fieldset>

<!-- Form Name -->
<legend>Add User</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="UserName">UserName</label>  
  <div class="col-md-4">
  <input id="UserName" name="UserName" type="text" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Password">Password</label>  
  <div class="col-md-4">
  <input id="Password" name="Password" type="password" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Fname">First Name</label>  
  <div class="col-md-4">
  <input id="Fname" name="Fname" type="text" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Lname">Last Name</label>  
  <div class="col-md-4">
  <input id="Lname" name="Lname" type="text" placeholder="" required="True"class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Address">Address</label>  
  <div class="col-md-4">
  <input id="Address" name="Address" type="text" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Social">Social</label>  
  <div class="col-md-4">
  <input id="Social" name="Social" type="text" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="DOB">Date of birth</label>  
  <div class="col-md-4">
  <input id="DOB" name="DOB" type="Date" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Gender">Gender</label>  
  <div class="col-md-4">
  <input id="Gender" name="Gender" type="text" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Submit"></label>
  <div class="col-md-4">
    <button id="Submit" name="Submit" class="btn btn-primary">Add</button>
  </div>
</div>

</fieldset>
</form>

</body>
</html>
