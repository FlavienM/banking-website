<html>
<head>
<style type=text/css>
</style>
</head>

<body>
<?php
session_start();
include_once("navbar.php");
include "dbconnect.php";

if(! $_SESSION['UserName']){
	echo "Login to access <a href= 'homepage.php'> Login </a><br>";
	exit;
}
$currentUserId = $_SESSION['UserId']; 

?>

<form class="form-horizontal" action="processTransaction.php">
<fieldset>

<legend>Transfer Money</legend>
<div class="form-group">
  <label class="col-md-4 control-label" for="SenderAccNo">ID of Account to send from</label>  
  <div class="col-md-4">
  <input id="SenderAccNo" name="SenderAccNo" type="number" min="0" placeholder="" required="True" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="ReceiverAccNo">Receiver Account Number</label>  
  <div class="col-md-4">
  <input id="ReceiverAccNo" name="ReceiverAccNo" type="number" min="0" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Account">Amount</label>  
  <div class="col-md-4">
  <input id="Amount" name="Amount" type="number" placeholder="" min="1" max="1000" required="True" class="form-control input-md">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Submit"></label>
  <div class="col-md-4">
    <button id="Submit" name="Submit" class="btn btn-primary">Transfer</button>
  </div>
</div>

</fieldset>
</form>