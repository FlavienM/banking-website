<html>
<head>
<title> 
FA Bank
</title>
</head>

<body>
<?php
include_once("navbar.php");
?>

<form class="form-horizontal" action = "AddAccount.php">
<fieldset>

<!-- Form Name -->
<legend>Add Account</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="UserId">User Id</label>  
  <div class="col-md-4">
  <input id="UserId" name="UserId" type="number" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="StartingBalance">Starting Balance</label>  
  <div class="col-md-4">
  <input id="StartingBalance" name="StartingBalance" type="number" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Type">Account Type</label>  
  <div class="col-md-4">
  <input id="Type" name="Type" type="text" placeholder="" required="True" class="form-control input-md">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Submit"></label>
  <div class="col-md-4">
    <button id="Submit" name="Submit" class="btn btn-primary">Add</button>
  </div>
</div>

</fieldset>
</form>

</body>
</html>
