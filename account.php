<!--
This page is used to display account information
-->

<html>
<head>

</style>
</head>

<body>
<?php
session_start();
include_once("navbar.php");
include "dbconnect.php";

if(! $_SESSION['UserName']){
	echo "Login to access <a href= 'homepage.php'> Login </a><br>";
	exit;
}

$currentUserId = $_SESSION['UserId'];
//echo " '$currentUserId' ";

$sql = "SELECT * FROM users WHERE UserId = '$currentUserId'";
$currentUser = $mysqli->query($sql);

$sql = "SELECT * FROM accounts WHERE UserId='$currentUserId'";
$accounts = $mysqli->query($sql);

$sql = "SELECT * FROM transactions WHERE SenderAccNo='$currentUserId' or ReceiverAccNo='$currentUserId' ORDER BY TransactionTime DESC LIMIT 10";
$recentTransactions = $mysqli->query($sql);

$sql = "SELECT * FROM DepositsWithdrawals WHERE AccountNo IN (SELECT AccountNo FROM accounts WHERE UserId='$currentUserId') ORDER BY dwDate DESC LIMIT 10";
$recentWithOrDep = $mysqli->query($sql);

$userrow = $currentUser -> fetch_assoc();

echo "<b>Hello ".$userrow['FName'].", Account Number = ".$userrow['UserId']." <br> here is a summary of your accounts<b><br><br>";

if($accounts->num_rows > 0){
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th>Account ID</th>
	    <th>Balance</th>
	<th>Type</th>
	<th>Expiration Date</th>
	<th></th>
	</tr>";
}
while ($row = $accounts -> fetch_assoc()){
	echo '<tr>
		<td style="width: 100px;" text-align: left;> '.$row['AccountNo'].' </td>
		 <td style="width: 100px;" text-align: left;> '.$row['Balance'].' </td>
		<td style="width: 80px;" text-align: left;> '.$row['Type'].' </td>
		<td style="width: 130px;" text-align: left;> '.$row['ExpDate'].' </td>';
if($row['Balance'] < 0){
	echo '<td style="width: 130px; text-align: left; color: red;">Your account is <b>overdrawn</b> </td>';
}
	echo '</tr>';
}
echo "</table><br><br>";
?>
<fieldset>
<form class="form-horizontal" action = "transactionForm.php">
<fieldset>
  <button type="submit" class="btn btn-primary">Make a transaction</button>
</form>
<br><br>
<form class="form-horizontal" action = "AccountInfo.php">
<fieldset>
  <button type="submit" class="btn btn-primary">Account Information</button>
</form>
<?php
echo "<h3>Recent Activity</h3>";
echo "<h4>Transactions</h4>";

if($recentTransactions->num_rows > 0){
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th>Time</th>
	    <th>Sender</th>
	<th>Receiver</th>
	<th>Amount</th>
	</tr>";
}
while ($row = $recentTransactions -> fetch_assoc()){
	$sql = "SELECT FName, LName FROM users WHERE UserId IN (SELECT DISTINCT UserId FROM accounts WHERE accountNo =".$row['ReceiverAccNo'].")";
	$ReceiverAccNo = $mysqli->query($sql);
	$ReceiverAccNo = $ReceiverAccNo -> fetch_assoc();
	$sql = "SELECT FName, LName FROM users WHERE UserId IN (SELECT DISTINCT UserId FROM accounts WHERE accountNo =".$row['SenderAccNo'].")";
	$SenderNames = $mysqli->query($sql);
	$SenderNames = $SenderNames -> fetch_assoc();
	echo '<tr>
		<td style="width: 180px;" text-align: left;> '.$row['TransactionTime'].' </td>
		 <td style="width: 150px;" text-align: left;> '.$SenderNames['FName']." ".$SenderNames['LName'].' </td>
		 <td style="width: 150px;" text-align: left;> '.$ReceiverAccNo['FName']." ".$ReceiverAccNo['LName'].' </td>
		<td style="width: 100px;" text-align: left;> '.$row['Amount'].' </td></tr>';
}
echo "</table>";

$sql = "SELECT SUM(Amount) AS Amount FROM transactions WHERE SenderAccNo='$currentUserId' ORDER BY TransactionTime DESC LIMIT 10";
$negative = $mysqli->query($sql)-> fetch_assoc();
$sql = "SELECT SUM(Amount) AS Amount FROM transactions WHERE ReceiverAccNo='$currentUserId' ORDER BY TransactionTime DESC LIMIT 10";
$positive = $mysqli->query($sql)-> fetch_assoc();
$total = $positive['Amount'] - $negative['Amount'];
echo "<h4>Total: ".$total."</h4><br><br>";

echo "<h4>Deposits and Withdrawals</h4>";
if($recentWithOrDep->num_rows > 0){
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th>Account ID</th>
	    <th>Amount</th>
	<th>Time</th>
	</tr>";
}
while ($row = $recentWithOrDep -> fetch_assoc()){
	echo '<tr>
		<td style="width: 100px;" text-align: left;> '.$row['AccountNo'].' </td>
		 <td style="width: 100px;" text-align: left;> '.$row['Amount'].' </td>
		<td style="width: 180px;" text-align: left;> '.$row['dwDate'].' </td>
	      </tr>';	
}
echo "</table>";

$sql = "SELECT SUM(Amount) AS Amount FROM DepositsWithdrawals WHERE AccountNo IN (SELECT AccountNo FROM accounts WHERE UserId=$currentUserId) ORDER BY dwDate DESC LIMIT 5";
$total = $mysqli->query($sql);
$total = $total -> fetch_assoc();
echo "<h4>Total: ".$total['Amount']."</h4>";
?>