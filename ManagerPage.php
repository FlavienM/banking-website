<html>
<head>
<title> 
FA Bank
</title>
</head>

<body>
<?php
session_start();
include_once("ManagerNav.php");
include "dbconnect.php";

/*if (!$_SESSION['Username']){
	$currentUserName = $_SESSION['Username'];
}*/
?>

<?php 
	if(! $_SESSION['Username']){
	echo "Login to access <a href= 'ManagerLoginForm.php'> Login </a><br>";
	exit;
	}
	echo "Welcome ".$_SESSION['Username']."";
	?>
	<legend> </legend>
<legend>Search for a user</legend>
<div>
<form class="form-horizontal" action = "AccountSearchForm.php">
<fieldset>
 <button type="submit" class="btn btn-primary">Search</button>
</form>
</div>

<legend>Add a new user</legend>
<div>
<form class="form-horizontal" action = "AddUserForm.php">
<fieldset>
  <button type="submit" class="btn btn-primary">Add</button>
</form>
</div>


<legend>Delete a user</legend>
<div>
<form class="form-horizontal" action = "DeleteUserForm.php">
<fieldset>
  <button type="submit" class="btn btn-primary">Delete</button>
</form>
</div>

<legend>Add Account</legend>
<div>
<form class="form-horizontal" action = "AddAccountForm.php">
<fieldset>
  <button type="submit" class="btn btn-primary">Add Account</button>
</form>
</div>

<legend>Deposit Money</legend>

<form class="form-horizontal" action = "DepositForm.php">
<fieldset>
  <button type="submit" class="btn btn-primary">Make Deposit</button>
</form>

<!--
<legend>Add a new user</legend>

<form class="form-horizontal" action = "AccountSearchForm.php">
<fieldset>
  <button type="submit" class="btn btn-primary">Add</button>
</form>



<legend>Add a new user</legend>

<form class="form-horizontal" action = "AccountSearchForm.php">
<fieldset>
  <button type="submit" class="btn btn-primary">Add</button>
</form> -->
<?php
$mysqli->close();
?>
</body>
</html>