
<html>
<head>

</style>
</head>

<body>
<?php
session_start();
include_once("navbar.php");
include "dbconnect.php";

if(! $_SESSION['UserName']){
	echo "Login to access <a href= 'homepage.php'> Login </a><br>";
	exit;
}

$currentUserId = $_SESSION['UserId'];
//echo " '$currentUserId' ";

$sql = "SELECT * FROM users WHERE UserId = '$currentUserId'";
$currentUser = $mysqli->query($sql);

echo "<b><h4>Your account information<h4><b><br><br>";

if($currentUser->num_rows > 0){
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th>UserName</th>
	    <th>First Name</th>
	<th>Last Name</th>
	<th>Address</th>
	<th>Social Security Number</th>
    <th>Date Of Birth</th>
    <th>Sex</th>
    <th>Account creation date</th>
	</tr>";
}
while ($row = $currentUser -> fetch_assoc()){
	echo '<tr>
		<td style="width: 150px;" text-align: left;> '.$row['UserName'].' </td>
		<td style="width: 150px;" text-align: left;> '.$row['FName'].' </td>
		<td style="width: 150px;" text-align: left;> '.$row['LName'].' </td>
		<td style="width: 250px;" text-align: left;> '.$row['Address'].' </td>
		<td style="width: 200px;" text-align: left;> '.$row['Social'].' </td>
		<td style="width: 130px;" text-align: left;> '.$row['DOB'].' </td>
		<td style="width: 50px;" text-align: left;> '.$row['SEX'].' </td>
		<td style="width: 200px;" text-align: left;> '.$row['MemberSince'].' </td></tr>';
}
echo "</table><br><br>";
?>

<form class="form-horizontal" action = "UpdateAccountInfo.php">
<fieldset>

<!-- Form Name -->
<legend>Update Information</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="UserName">UserName</label>  
  <div class="col-md-4">
  <input id="UserName" name="UserName" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Password">Password</label>  
  <div class="col-md-4">
  <input id="Password" name="Password" type="password" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Fname">First Name</label>  
  <div class="col-md-4">
  <input id="Fname" name="Fname" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Lname">Last Name</label>  
  <div class="col-md-4">
  <input id="Lname" name="Lname" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Address">Address</label>  
  <div class="col-md-4">
  <input id="Address" name="Address" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Gender">Gender</label>  
  <div class="col-md-4">
  <input id="Gender" name="Gender" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Submit"></label>
  <div class="col-md-4">
    <button id="Submit" name="Submit" class="btn btn-primary">Update</button>
  </div>
</div>

</fieldset>
</form>

</body>
</html>